import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login'
import Home from '../components/Home'
import Users from '../components/user/Users'
import Welcome from '../components/Welcome'
import Rights from '../components/power/Rights'
import Roles from '../components/power/Roles'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/login'
    }, {
      path: '/login',
      component: Login
    }, {
      path: '/home',
      component: Home,
      redirect: '/welcome',
      children: [
        {
          path: '/welcome',
          component: Welcome
        }, {
          path: '/users',
          component: Users
        }, {
          path: '/rights',
          component: Rights
        }, {
          path: '/roles',
          component: Roles
        }
      ]
    }
  ]
})

/**
 * 挂载路由导航守卫
 * to 将要访问的路径
 * from 从哪个路径跳转过来
 * next 是一个函数 表示放行
 *    next()  放行    next('/路径') 强制跳转
 */
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  // 先获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
